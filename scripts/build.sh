#!/bin/bash

if [[ ! -d "wspace" ]];then
    mkdir -p wspace
fi

cd wspace

if [[ ! -d "personal-website" ]];then
    git clone https://github.com/rnjane/personal-website.git
else
    rm -r personal-website
    git clone https://github.com/rnjane/personal-website.git
fi

cd personal-website

pip3 install -r requirements.txt
