#!/bin/bash

if [[ ! -d "personal-website" ]];then
    git clone https://github.com/rnjane/personal-website.git
else
    rm -r personal-website
    git clone https://github.com/rnjane/personal-website.git
fi

cd personal-website

gunicorn --bind 0.0.0.0:5000 wsgi:app
